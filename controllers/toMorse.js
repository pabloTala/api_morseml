const codificador = require('../impl/toMorseImpl.js');

function getTextFromRec(req) {
  const morse = {
    morseText: req.body.text,
  };

  return morse;
}

async function post(req, res, next) {
  try {
    let morse = getTextFromRec(req);

    // console.log(morse.morseText);

    morse = await codificador.translate(morse.morseText);

    res.status(201).json(morse);
  } catch (err) {
    next(err);
  }
}

module.exports.post = post;
