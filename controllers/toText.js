const decodidificador = require('../impl/toTextImpl.js');

function getMorseTextFromRec(req) {
  const morse = {
    morseText: req.body.text,
  };

  return morse;
}

async function post(req, res, next) {
  try {
    let morse = getMorseTextFromRec(req);

    // console.log(morse.morseText);

    morse = await decodidificador.translate(morse.morseText);

    res.status(201).json(morse);
  } catch (err) {
    next(err);
  }
}

module.exports.post = post;
