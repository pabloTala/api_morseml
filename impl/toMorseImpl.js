

async function translate(morseCode) {
    function decodeMorseLetter(letter) {

        return MORSE_CODE_INVERSO[letter];
    }

    function decodeMorseWord(word) {

        return word.split('').map(decodeMorseLetter).join(' ');
    }
    return morseCode.toUpperCase().trim().split(' ').map(decodeMorseWord).join('   ');
};

module.exports.translate = translate;

 MORSE_CODE_INVERSO = {
 "A"  :".-"	    ,
 "B"  :"-..."	,
 "C"  :"-.-."	,
 "D"  :"-.."	    ,
 "E"  :"."		,
 "F"  :"..-."	,
 "G"  :"--."	    ,
 "H"  :"...."	,
 "I"  :".."	    ,
 "J"  :".---"	,
 "K"  :"-.-"	    ,
 "L"  :".-.."	,
 "M"  :"--"	    ,
 "N"  :"-."	    ,
 "O"  :"---"	    ,
 "P"  :".--."	,
 "Q"  :"--.-"	,
 "R"  :".-."	    ,
 "S"  :"..."	    ,
 "T"  :"-"		,
 "U"  :"..-"	    ,
 "V"  :"...-"	,
 "W"  :".--"	    ,
 "X"  :"-..-"	,
 "Y"  :"-.--"	,
 "Z"  :"--.."	,
 "0"  :"-----"	,
 "1"  :".----"	,
 "2"  :"..---"	,
 "3"  :"...--"	,
 "4"  :"....-"	,
 "5"  :"....."	,
 "6"  :"-...."	,
 "7"  :"--..."	,
 "8"  :"---.."	,
 "9"  :"----."
};
