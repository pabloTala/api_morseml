const express = require('express');
const router = new express.Router();
const totext = require('../controllers/toText.js');
const toMorse = require('../controllers/toMorse.js');

router.route('/toText')
  .post(totext.post)

router.route('/toMorse')
  .post(toMorse.post)

module.exports = router;
